package sheridan;

public class Palindrome {
	
	public static boolean isPalindrome( String input ) {
		input = input.toLowerCase().replace(" ","");
		for ( int i = 0 , j = input.length( ) - 1 ; i < j ; i++ , j-- ) {
			if ( input.charAt( i ) !=  input.charAt( j ) ) {
				return false;
			}
		}
		
		return true;
	}

	public static void main(String[] args) {
		
		System.out.println( "is anna palindrome? " + Palindrome.isPalindrome( "anna" ) );
		System.out.println( "is anna palindrome? " + Palindrome.isPalindrome( "race car" ) );
		System.out.println( "is anna palindrome? " + Palindrome.isPalindrome( "taco cat" ) );
		System.out.println( "is anna palindrome? " + Palindrome.isPalindrome( "this is not palindrome" ) );

	}

}
